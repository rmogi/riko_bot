<?php
/**
 *  Main Header
*/

// Error
ini_set('display_errors', 1);

//Debug
function d ($var) {
    echo '<pre>';
    var_dump($var);
    echo '</pre>';
}

// Application Root
define('APP', dirname(__FILE__).'/app');

// Vendors Root
define('LIBRARY', dirname(__FILE__).'/vendors');

// Config
require_once 'app/conf/config.php';

// Bot Frame Work
require_once 'lib/Model/Model.class.php';
require_once 'lib/Action/Action.class.php';
require_once 'lib/Controller/Controller.class.php';

// Twitter OAuth
TwitterModel::OAuth($CONF['Twitter']);

// Database
MDB2::connect($CONF['Dsn']);
