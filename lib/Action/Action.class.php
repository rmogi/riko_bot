<?php
/**
 *
 *
*/

spl_autoload_register('Action::Autoload');

class Action extends Model {

    public static function Autoload ($class_name) {
        if (preg_match('/(Model|Action)/', $class_name, $match)) {
            require_once APP."/{$match[1]}/".$class_name.'.class.php';
        }
    }

    public function getConfig ($key) {
        return $this->conf[$key];
    }

}
