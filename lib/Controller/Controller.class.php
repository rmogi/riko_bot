<?php
/**
 *
 *
*/
class Controller {

    public $conf;

    public function __construct ($conf = null) {
        $this->conf = $conf;
    }

    public function dispatch ($target) {
        $target = $target.'Action';
        $action = new $target($this->conf);
        $action->execute();
    }

}
