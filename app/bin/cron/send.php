<?php
/**
 *  Send
 *  タイムラインから特定ワードを
 *  含むツイートにリプライを送る
 *
 *  @author R.SkuLL
 *  @version 1.0
 *
 *  Copyright (c) 2012-2013 Necoha Bot Project
*/
require_once dirname(__FILE__).'/../../../main_header.php';
$Send = new Controller($CONF);
$Send->dispatch('Send');
