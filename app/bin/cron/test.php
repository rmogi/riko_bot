<?php
/**
 *  Twitter Bot Test
 *  @author R.SkuLL
*/
require_once dirname(__FILE__).'/../../../main_header.php';

$text = $_POST['text'];

if (!empty($text)) {

    $reply = array(
        'screen_name' => 'r_skull',
        'user_id' => '234781191',
        'status_id' => '1234567890',
        'text' => $text
    );

    MDB2::connect($CONF['Dsn']);
    $Reply = new ReplyModel();
    $result = $Reply->Response($reply);

}

?>

<!DOCTYPE html>
<html lang="ja">
<head>
    <title>Bot Generate Test</title>
    <meta charset="utf-8">
    <style>
        textarea {
            width: 300px;
            height: 100px;
            font-size: 18px;
            margin-bottom: 5px;
        }
        input {
            display: block;
            border: 1px solid #555;
            border-radius: 3px;
            background: #F5F5F5;
            width: 305px;
            height: 50px;
        }
        div {
            background: #EEE;
            border: 1px solid #BBB;
            margin-top: 10px;
            padding: 10px;
        }
    </style>
</head>
<body>
    <h1>猫葉おしゃべりシュミレーション</h1>
    <form action="" method="post">
        <textarea name="text"><?php echo $text ?></textarea>
        <input type="submit" value="話しかける">
    </form>
    <div>問い：<?php echo $text; ?></div>
    <div>応答：<?php echo $result; ?></div>
</body>
</html>

