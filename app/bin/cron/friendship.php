<?php
/**
 *  Friendship
 *  完全相互フォロー
 *
 *  @author R.SkuLL
 *  @version 1.0
 *
 *  Copyright (c) 2012-2013 Necoha Bot Project
*/
require_once dirname(__FILE__).'/../../../main_header.php';
$Friendship = new Controller();
$Friendship->dispatch('Friendship');
