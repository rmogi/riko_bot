<?php
/**
 *  Schedule
 *  スケジュール管理
 *
 *  @author R.SkuLL
 *  @version 1.0
 *
 *  Copyright (c) 2012-2013 Necoha Bot Project
*/
require_once dirname(__FILE__).'/../../../main_header.php';
$schedule = new Controller($CONF);
$schedule->dispatch('Schedule');
