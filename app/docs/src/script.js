$(function () {

    var w = window, d = document,
    hash = w.location.hash.substr( 1 );

    // hash の監視を開始
    tm.HashObserver.enable();

    // 初期ハッシュのチェック
    pageSelect( hash );

    // ハッシュ変更イベントを登録
    d.addEventListener( 'changehash', function ( e ) {
        var hash = e.hash.substr( 1 );
        pageSelect( hash );
    }, false );

    // ページセレクト
    function pageSelect ( h ) {
        var hash = checkHash( h );
        if ( hash === false ) return;
        changePage( hash );
    }

    // 指定ページの読み込み
    function changePage ( page ) {
        if ( page == '') page = 'home';
        $.ajax({
            type: 'GET',
            url: 'contents/' + page + '.html',
            success: function ( content ) {
                $( '#main' ).html( content ).hide();
                $( '#main' ).slideDown( 'first' );
            }
        });
    }

    // 有効ハッシュかチェック
    function checkHash ( hash ) {
        var hashList = new Array( '', 'home', 'info', 'commu', 'club');
        for ( var i = 0,len = hashList.length; i < len; i++ ) {
            if ( hash == hashList[ i ] ) {
                highlight( i );
                return hash;
            }
        }
        return false;
    }

    function highlight ( i ) {
        $( 'nav a:not(:eq('+ i +'))' ).removeClass( 'navi' );
        $( 'nav a:eq('+ i++ +')' ).addClass( 'navi' );
    }

    // Menu
    /*
    $( '#header' ).hide();
    $( 'nav a' ).hide();
    $( '#header' ).slideDown( 'first' );
    $( '#header img' ).hide().fadeIn( 'first', function () {
        var i = 0,
        time = setInterval( function () {
            $( 'nav a:eq('+ i++ +')' ).slideDown( 'first' );
            if ( i == 8 ) clearInterval( time );
        }, 100 );
    });
    */

});
