<?php
/**
 *  Send Model
 *  @author R.SkuLL
*/

class SendModel extends Model {

    public $Twitter;

    public function __construct ($conf) {
        parent::__construct($conf);
        $this->Twitter = TwitterModel::singleton();
    }

    /**
     *  タイムラインにリプライを送る
     *  @param int count タイムラインの取得数
    */
    public function SendReply () {

        // 固定辞書のID
        $ids = $this->conf['ids'];

        $Reply = new ReplyModel();
        $Markov = new MarkovModel();

        // タイムラインを取得
        $list = $this->SendList($this->conf['count']);

        foreach ($list as $val) {

            $statusId = $val['status_id'];
            $text = $val['text'];

            // 固定辞書にあるか
            $tweet = $Reply->FixedPhrase($text);

            // 定義リストにIDが存在するか
            if ($tweet && in_array($tweet['id'], $ids)) {

                // 応答を生成
                $speak = $Reply->Response($val);

                // Post
                $result = $this->Twitter->Post($speak, $statusId);
                if (empty($result->error)) {
                    echo $speak;
                    ReplyDbModel::RegistTweet($statusId);
                } else {
                    echo $result->error;
                }

            }

        }

    }

    /**
     *  返信するツイートのリストを返す
     *  @param Int count 取得するタイムライン数
     *  @return Array replyList リプライ対象のリスト
    */
    private function SendList ($count = 10) {

        $list = array();
        $users = array();
        $myId = $this->Twitter->getMyId();

        // ツイッターから自分宛のリプライを取得
        $mentions = $this->Twitter->Timeline('friends_timeline', $count);

        // 順番に取得して解析
        foreach ($mentions as $reply) {

            // もし自分の発言だったらスキップ
            if (($reply->user->id_str == $myId) ||
            (preg_match('/@[\w]+/', $reply->text))) continue;

            // 返信してなかったらリストに追加
            if (ReplyDbModel::ReplyCheck($reply->id_str)) {

                // ツイート時間を日本時間に直す
                $date = new DateTime($reply->created_at);
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                $date = $date->format('Y/m/d');
                $now = date('Y/m/d');

                // ツイートとの差が１日以上ならスキップ
                $diff = (int) date('d', strtotime($now) - strtotime($date));
                if ($diff > 1) continue;

                $userId = $reply->user->id_str;

                // 既にリプライユーザが存在したら古い方を返さないようにする
                if (in_array($userId, $users)) continue;
                $users[] = $userId;

                $list[$userId] = array(
                    'screen_name' => $reply->user->screen_name,
                    'user_id' => $userId,
                    'status_id' => $reply->id_str,
                    'text' => $reply->text
                );
            }

        }

        return $list;

    }


}
