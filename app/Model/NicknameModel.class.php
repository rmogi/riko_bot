<?php
/**
 *  Nickname Model
 *  @author R.SkuLL
*/

final class NicknameModel extends Model {

    public $Db;

    /**
     *  Constructer
    */
    public function __construct () {
        $this->Db = MDB2::singleton();
    }

    /**
     *  虫食い文字を名前に変換
     *  @param string user screnn_name
     *  @param int id twitter_id
     *  @param string speak 発言
     *  @return string 生成された文章
    */
    public function Replace ($user, $id, $speak) {

        // ニックネームを取得
        $nickname_flg = false;
        $nickname = $this->getNickname($id);

        // 登録済なら置換、なければｽｸﾘｰﾝﾈｰﾑ
        if (!empty($nickname)) {
            $nickname_flg = true;
            $replyto = $user;
            $user = $nickname;
        } else {
            $user .= 'たん';
        }

        // 変換
        $speak = preg_replace('/%NAME%/', $user, $speak);
        if ($nickname_flg) $speak = $replyto.$speak;

        return $speak;

    }

    /**
     *  ニックネームを取得
     *  @param int id Twitter_id
     *  @return mixed ニックネーム
    */
    public function getNickname($id) {
        $sql = 'SELECT nickname FROM nickname WHERE twitter_id = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($id))->fetchCol();
        return !empty($result[0]) ? $result[0] : false;
    }

    /**
     *  ニックネームを登録
     *  @param int id Twitter_id
     *  @param string nickname ニックネーム 
    */
    public function addNickname ($id, $nickname) {
        $sql = "INSERT INTO nickname (twitter_id, nickname) VALUES('$id', '$nickname')";
        if ($this->checkNickname($id)) {
            $sql = "UPDATE nickname SET nickname = '$nickname' WHERE twitter_id = '$id'";
        }
        $this->Db->exec($sql);
        return "じゃぁこれから\"{$nickname}\"って呼ぶねーッ";
    }

    /**
     *  すでに登録されているか
     *  @param int id Twitter_id
     *  @return boolean
    */
    private function checkNickname ($id) {
        $sql = 'SELECT COUNT(*) FROM nickname WHERE twitter_id = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($id))->fetchCol();
        return !empty($result[0]) ? true : false;
    }

}

