<?php
/**
 *  Reply Model
 *  @author R.SkuLL
*/

final class ReplyModel extends Model {

    public $Db;
    public $Twitter;
    public $Nickname;

    /**
     *  Constructer
    */
    public function __construct () {
        $this->Db = MDB2::singleton();
        $this->Twitter = TwitterModel::singleton();
        $this->Nickname = new NicknameModel();
    }

    /**
     *  問いかけに対する応答を返す
     *  @param string text 話しかけた言葉
     *  @return string 応答
    */
    public function Response ($reply) {

        $user = '@'.$reply['screen_name'].' ';
        $id = $reply['user_id'];
        $text = $reply['text'];

        // 固定辞書に登録されていたら返す
        $result = $this->FixedPhrase($text);
        if (!empty($result)) {

            $speak = $result['speak'];

            // 機能ワードだったら実行
            switch ($speak) {
                case 'Nickname':
                    $nickname = $result['match'][1];
                    $speak = $this->Nickname->addNickname($id, $nickname);
                    break;
                case 'Weather':
                    $Weather = new WeatherModel();
                    $speak = $Weather->Request($text);
                    break;
            }

            // 名前を置き換える
            if (preg_match('/%NAME%/',  $speak)) {
                $speak = $this->Nickname->Replace($user, $id, $speak);
            } else {
                $speak = $user.$speak;
            }

            return $speak;

        }

        // 話しかけられた言葉を学習
        $Markov = new MarkovModel();
        $Markov->Study($text);

        // 辞書になければマルコフ連鎖でしゃべる
        return $user.$Markov->Generate($text);

    }

    /**
     *  返信するツイートのリストを返す
     *  @param Int count 取得するタイムライン数
     *  @return Array replyList リプライ対象のリスト
    */
    public function ReplyList ($coun) {

        $list = array();

        // ツイッターから自分宛のリプライを取得
        $mentions = $this->Twitter->Timeline('mentions', $count);

        // 順番に取得して解析
        foreach ($mentions as $reply) {

            // もし自分の発言だったらスキップ
            if ($reply->user->id_str == $this->Twitter->getMyId()) continue;

            // 返信してなかったらリストに追加
            if (ReplyDbModel::ReplyCheck($reply->id_str)) {

                // ツイート時間を日本時間に直す
                $date = new DateTime($reply->created_at);
                $date->setTimezone(new DateTimeZone('Asia/Tokyo'));
                $date = $date->format('Y/m/d');
                $now = date('Y/m/d');

                // ツイートとの差が１日以上ならスキップ
                $diff = (int) date('d', strtotime($now) - strtotime($date));
                if ($diff > 1) continue;

                $userId = $reply->user->id_str;

                // 既にリプライユーザが存在したら古い方を返さないようにする
                if (array_key_exists($userId, $list)) {
                    ReplyDbModel::RegistTweet($reply->id_str);
                    continue;
                }

                $text = preg_replace('/@[\w]+ /', '', $reply->text);
                $list[$userId] = array(
                    'screen_name' => $reply->user->screen_name,
                    'user_id' => $userId,
                    'status_id' => $reply->id_str,
                    'text' => $text
                );
            }

        }

        return $list;

    }

    /**
     *  固定辞書から応答を検索
     *  @param string text 話しかけた言葉
     *  @return array 応答とマッチ文字列
    */
    public function FixedPhrase ($text) {
        $sql = 'SELECT * FROM fixed_phrase';
        $result = $this->Db->query($sql);
        while ($row = $result->fetchRow(MDB2_FETCHMODE_ASSOC)) {
            if (preg_match($row['pattern'], $text, $match)) {
                $speaks = explode('|', $row['talk']);
                return array(
                    'id' => $row['id'],
                    'speak' => $speaks[mt_rand(0, count($speaks)-1)],
                    'match' => $match
                );
            }
        }
        return false;
    }

}

