<?php
/**
 *  Yahoo Keyphrase Model
 *  @author R.SkuLL
*/

final class Yahoo_KeyphraseModel extends Model {

    const API_KEY = 'PFH7HOuxg65cOpF6I8Otuvd07_2tsYEvAMXiWi4JTBJYl6KxD6STH6zzlxayHPTRQ5HF';
    const API_URL = 'http://jlp.yahooapis.jp/KeyphraseService/V1/extract';

    public $xml;

    /**
     *  リクエストを受け取る
     *  @param string text 文章
    */
    public function Request ($text) {

        $params = array(
            'appid' => self::API_KEY,
            'sentence' => $text,
            'output' => 'xml'
        );

        $api = new Web_ApiModel();
        $this->xml = $api->Request(self::API_URL, $params);
        return $this->xml->Result;

    }

    /**
     *  一番重要度の高い言葉を返す
    */
    public function getKeyphrase () {
        return $this->xml->Result->Keyphrase;
    }

    /**
     *  重要度
    */
    public function Score () {
        return $this->xml->Result->Score;
    }

}
