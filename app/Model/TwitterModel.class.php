<?php
/**
 *  Twitter Model
 *  @author R.SKuLL
*/
final class TwitterModel extends Model {

    // This Class Instance;
    private static $instance;

    // Twitter OAuth
    private static $OAuth;

    /**
     *  Clone
    */
    private final function __clone () {
        return new RuntimeException('Clone is not allowed against '.get_class($this));
    }

    /**
     *  Instance Twitter OAuth
     *  @param array conf キートークン
     *  @return object TwitterOAuth
    */
    public static function OAuth ($conf) {
        Model::Library('twitterOAuth', 'twitteroauth');
        self::$OAuth = new TwitterOAuth($conf['comsumerKey'], $conf['comsumerKeySecret'], $conf['accessToken'], $conf['accessTokenSecret']);
        self::$instance = new TwitterModel();
        return self::$instance;
    }

    /**
     *  Singleton get instance
    */
    public static function singleton () {
        if (!isset(self::$OAuth)) return new RuntimeException('This class is not found');
        if (!isset(self::$instance)) self::$instance = new Twitter();
        return self::$instance;
    }

    /**
     *  リクエストを送信する
     *  @param string url リクエストURL
     *  @param string method メソッド
     *  @param array opt オブション
     *  @return object リクエスト結果
     *
    */
    private function Request ($url, $method = 'POST', $opt = array()) {
        $req = self::$OAuth->OAuthRequest('http://api.twitter.com/1/'.$url, $method, $opt);
        if ($req) { $result = $req; } else { $result = null; }
        return $result;
    }

    /**
     *  ツイッターに投稿
     *  @param string text ポスト内容
     *  @param integer repto 返信相手のID
     *  @return object リクエスト結果
    */
    public function Post ($text, $repto = null) {
       $opt = array();
       $opt['status'] = $text;
       $opt['in_reply_to_status_id'] = $repto;
       $result = $this->Request('statuses/update.json', 'POST', $opt);
       return json_decode($result);
    }

    /**
     *  タイムラインを取得
     *  @param string type 取得タイプ
     *  @param integer count 取得数
     *  @return object リクエスト結果
    */
    public function Timeline ($type, $count = 30) {
        $opt = array();
        $opt['count'] = $count;
        $req = $this->Request('statuses/'.$type.'.json', 'GET', $opt);
        $result = json_decode($req);
        if (!is_array($result)) { die('Error'); }
        return $result;
    }

    /**
     *  ユーザをフォロー・リムーブ
     *  @param integer uid ユーザのID
     *  @param boolean flg フォーローかリムーブか
     *  @return object リクエスト結果
    */
    public function Follow ($uid, $flg = true) {
        $opt = array();
        $opt['id'] = $uid;
        $result = $this->Request('friendships/'.($flg?'create':'destroy').'.json', 'POST', $opt);
        return json_decode($result);
    }

    /**
     *  フォロワーを取得
     *  @param int uid 取得対象(無指定で自分)
     *  @return objct フォロワーのIDリスト
    */
    public function Followers ($uid = null) {
        $opt = array();
        $opt['user_id'] = $uid;
        $result = $this->Request('followers/ids.json', 'GET', $opt);
        return json_decode($result, true);
    }

    /**
     *  フレンドを取得
     *  @param int uid 取得対象(無指定で自分)
     *  @return objct フレンドーのIDリスト
    */
    public function Friends ($uid = null) {
        $opt = array();
        $opt['user_id'] = $uid;
        $result = $this->Request('friends/ids.json', 'GET', $opt);
        return json_decode($result, true);
    }

    /**
     *  リプライをくれたユーザリスト
     *  @param strin stid ステータスID
     *  @return jsonObject json 
    */
    public function Replys ($stid) {
        $api = "https://api.twitter.com/1/related_results/show/{$stid}.json";
        $result = @file_get_contents($api);
        $json = json_decode($result);
        if (!$json[0]) return false;
        return $json;
    }

    /**
     *  @return string twitterId
    */
    public function getMyId () {
        $result = self::$OAuth->token;
        if (preg_match('/(^[0-9]+)\-/', $result->key, $match)) {
            return $match[1];
        }
    }

}
