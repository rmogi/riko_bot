<?php
/**
 *  Web API Model
 *  @author R.SkuLL
*/

final class Web_ApiModel extends Model{

    public $result;

    /**
     *  リクエストを受け取る
     *  @param string url URL
     *  @param array params パラメータ
    */
    public function Request ($url, $params) {
        $encode_params = array();
        foreach ($params as $key=>$val) {
            $encode_params[] = urlencode($key) . '=' . urlencode($val);
        }
        $req = $url . '?' . join('&', $encode_params);
        return $this->result = $this->LoadFile($req);
    }

    /**
     *  結果を取得する
     *  @param string req リクエストURL
    */
    private function LoadFile ($req) {
        //return $this->getFile($req);
        return simplexml_load_file($req);
    }

    /**
     *  学校のプロキシ経由で取得
     *  これがないと取得できない！！
    */
    private function getFile ($req) {
        $proxy = array(
            'http' => array(
                'proxy' => 'tcp://cache1.hac.neec.ac.jp:8080',
                'request_fulluri' => true
            )
        );
        $proxy_context = stream_context_create($proxy);
        $file = file_get_contents($req, false, $proxy_context);
        return simplexml_load_string($file);
    }

}
