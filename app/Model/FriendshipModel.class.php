<?php
/**
 *  Friendship Model
 *  @author R.SkuLL
*/

final class FriendshipModel extends Model {

    public $Twitter;

    /**
     *  Constucter
    */
    public function __construct () {
        $this->Twitter = TwitterModel::singleton();
    }

    /**
     *  相互フォローの差
     *  @return array Diff
    */
    public function Diff () {

        // フレンドとフォロワー取得
        $friends = $this->Twitter->Friends();
        $followers = $this->Twitter->Followers();

        // フォローしてないリスト
        $follow = array_diff($followers['ids'], $friends['ids']);
        // リムられたリスト
        $remove = array_diff($friends['ids'], $followers['ids']);

        return array('follow' => $follow, 'remove' => $remove);

    }

}
