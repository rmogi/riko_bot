<?php
/**
 *  Yahoo Morph API Model
 *  @auhtor R.SkuLL
*/

final class Yahoo_MorphModel extends Model {

    const API_KEY = 'PFH7HOuxg65cOpF6I8Otuvd07_2tsYEvAMXiWi4JTBJYl6KxD6STH6zzlxayHPTRQ5HF';
    const API_URL = 'http://jlp.yahooapis.jp/MAService/V1/parse';

    public $xml;

    /**
     *  リクエストを受け取る
     *  @param string text 文章
     *  @return object 解析結果
    */
    public function Request ($text, $filter = false) {

        $params = array(
            'appid' => self::API_KEY,
            'sentence' => $text,
            'results' => 'ma',
        );

        if ($filter) $params['filter'] = $filter;

        $api = new Web_ApiModel();
        $this->xml = $api->Request(self::API_URL, $params);
        return $this->xml->ma_result->word_list;

    }

    /**
     *  解析要素数
    */
    public function TotalCount () {
        return $this->xml->ma_result->total_count;
    }

    /**
     *  要素リスト
    */
    public function Words () {
        return $this->xml->ma_result->word_list->word;
    }

    /**
     *  フィルターにかかった数
    */
    public function FilterCount () {
        return $this->xml->ma_result->filtered_count;
    }

    /**
     *  フィルターにかかった最初の文字
     *  @return string ワード
    */
    public function OneWord () {
        if ($this->FilterCount() >= 1) {
            return $this->Words()->surface;
        }
        return false;
    }

}
