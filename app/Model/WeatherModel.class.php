<?php
/**
 *  Weather Model
 *  @author R.SkuLL
*/

final class WeatherModel extends Model {

    const RSS = 'http://weather.livedoor.com/forecast/rss/13/63.xml';

    /**
     *  リクエストを送信する
     *  @param string text 発言
     *  @return string 応答
    */
    public function Request ($text) {

        $api = new Web_ApiModel();
        $xml =  $api->Request(self::RSS, array());

        // 初期値：今日
        $num = 1;
        // エラーメッセージ
        $error = '';

        // 時間指定の天気予報パターン
        $pattern = array(
            '/今日|きょう/' => 0,
            '/明日|あした/' => 1,
            '/明後日|あさって/' => 2,
            '/明々後日|しあさって/' => 3,
            '/([0-9])日後/' => 4,
            '/([0-9]{1,2})日/' => 5
        );

        foreach ($pattern as $key=>$val) {

            // 全角数値を半角に変換
            $text = mb_convert_kana($text, 'n');

            if (preg_match($key, $text, $match)) {

                switch ($val) {

                    // 今日
                    case 0;
                        $num = 1;
                        break;

                    // 明日
                    case 1:
                        $num = 2;
                        break;

                    // 明後日
                    case 2:
                        $num = 3;
                        break;

                    // 明々後日
                    case 3:
                        $num = 4;
                        break;

                    // 何日後指定
                    case 4:
                        $num = (int) $match[1] + 1;
                        if ($match[1] >= 9) $error = '７日後までしか取得できないお！';
                        break;

                    // 日付指定
                    case 5:
                        $day = $match[1];
                        // 日付がなかったとき
                        if (($last = date('t')) < $day) {
                            $error = "今月は{$last}日までだよっ！";
                        } else
                        // 今日
                        if (date('j') == $day) {
                            $num = 1;
                        } else
                        // 今月の日付
                        if (($today = date('j')) < $day) {
                            if (($num = $day - $today + 1) >= 9) $error = '今月の'.($today+7).'までしか取得できないお！';
                        // 日付指定
                        } else {
                            $num = strtotime(date('m/'.$day, strtotime('+1 month'))) - strtotime(date('m/j'));
                            $num = $num / (60 * 60 * 24) + 1;
                            if ($num >= 9) {
                                $max = date('j', strtotime('+7 day'));
                                $error = "来月{$max}日までしか取得できないお！";
                            }
                        }
                        break;

                }

                break;

            }

        }

        $result = $xml->channel->item[$num]->description;
        if (!empty($error)) return $error;

        // 語尾変更
        $result = str_replace('でしょう。', 'だって！', $result);
        return '東京の'.$result;

    }

}

