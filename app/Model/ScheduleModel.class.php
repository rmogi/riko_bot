<?php
/**
 *  Schedule Model
 *  @author R.SkuLL
*/

final class ScheduleModel extends Model {

    public $conf;
    public $Db;
    public $Twitter;

    /**
     *  Constructer
     *  @param array conf Config
    */
    public function __construct ($conf) {
        $this->conf = $conf;
        $this->Db = MDB2::singleton();
        $this->Twitter = TwitterModel::singleton();
    }

    /**
     *  実行
    */
    public function execute () {

        $date = date('H-i');
        foreach ($this->conf[1] as $key=>$val) {

            // 実行時間の範囲を取得
            list($start, $end) = explode('-', $val);

            // スタート時間に達していたら
            if ($start <= $date && $end >= $date) {

                // エンドタイムとの差
                $diff = gmdate('i', strtotime($end) - strtotime($date));

                // 終了時間をすぎたら、またはランダムで実行
                // 差が10になったら強制的に実行
                if ($diff <= 10 || $this->RandomExec()) {

                    switch ($key) {
                        case 0:
                            $this->Morning();
                            break;
                        case 1:
                            $this->Daytime();
                            break;
                        case 2:
                            $this->Night();
                            break;
                        case 3:
                            $this->UpdateFlg('Morning', 0);
                            $this->UpdateFlg('Daytime', 0);
                            $this->UpdateFlg('Night', 0);
                            break;
                    }

                }

                break;
            }

        }
    }

    /**
     *  起床時の処理
    */
    private function Morning () {

        if ($this->CheckFlg('Morning')) return;

        // 寝た時のツイートに対しての反応
        $talk = '';
        $tweet = $this->Twitter->Timeline('user_timeline', 1);
        $tweet = $tweet[0];

        // おやすみツイートか念のためチェック
        if (preg_match('/おや/', $tweet->text)) {
            echo 'OK';
            $list = $this->Replys($tweet->id_str);
            $talk = $list['users'].'おはよう！';
            $this->Twitter->Post($talk);
            foreach ($list['status_ids'] as $val) {
                ReplyDbModel::RegistTweet($val);
            }
            sleep(3);
        }

        $list = array(
            'おはようございます〜。',
            'おはようっす〜。',
            'おはでーす。',
            'おはっす〜。'
        );
        $talk = $list[mt_rand(0, count($list)-1)];
        $this->Twitter->Post($talk);
        sleep(3);

        $weather = new WeatherModel();
        $talk = $weather->Request('今日');
        $this->UpdateFlg('Morning', 1);
        $this->Twitter->Post($talk);

    }

    /**
     *  お昼の処理
    */
    private function Daytime () {

        if ($this->CheckFlg('Daytime')) return;

        $list = array(
            'こんちわーーッお昼っすよおおおおおお',
            'お昼ご飯なんにしよっかなぁぁああああ',
            'お腹すいたー。お昼になりましたー',
            'お昼ご飯の時間だよーーっちゃんと食べろよ！'
        );
        $talk = $list[mt_rand(0, count($list)-1)];
        $this->UpdateFlg('Daytime', 1);
        $this->Twitter->Post($talk);

    }

    /**
     *  就寝時の処理
    */
    private function Night () {

        if ($this->CheckFlg('Night')) return;
        $weather = new WeatherModel();
        $talk = $weather->Request('今日');
        $this->Twitter->Post($talk);
        sleep(3);

        $list = array(
            'ではおやすみなさい…',
            'ってことで寝ます！おやすみっす〜',
            'ということで寝るよーッおやすみ！',
            '…さて寝ます。おやすみー。'
        );
        $talk = $list[mt_rand(0, count($list)-1)];
        $this->UpdateFlg('Night', 1);
        $this->Twitter->Post($talk);

    }

    /**
     *  ブログの更新チェック
    */
    private function Blogcheck () {
    }

    /**
     *  実行済みでないかチェック
     *  @param string event イベント名
    */
    public function CheckFlg ($event) {
        $sql = 'SELECT flg FROM schedule WHERE event = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($event))->fetchCol();
        return $result[0];
    }

    /**
     *  フラグをたてる
     *  @param string event イベント名
     *  @param int flg フラグ
    */
    private function UpdateFlg ($event, $flg) {
        $sql = 'UPDATE schedule SET flg = ? WHERE event = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($flg, $event));
    }

    /**
     *  一定確率で実行する
     *  @return boolean
    */
    private function RandomExec () {
        if (mt_rand(0, 6) === 1) return true;
        return false;
    }

    /**
     *  おやすみに対するリプライをまとめて返す
     *  @param string stid ステータスID
     *  @return string ユーザリスト
    */
    private function Replys ($stid) {
        $users = array();
        $json = $this->Twitter->Replys($stid);
        if (!$json[0]) return $users;
        foreach ($json[0]->results as $val) {
            $users[] = '@'.$val->value->user->screen_name.' ';
            $status_ids[] = $val->value->id_str;
        }
        $list = array_unique($users);
        return array(
            'users' => join('', $list),
            'status_ids' => $status_ids
        );
    }

}
