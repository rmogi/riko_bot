<?php
/**
 *  Markov Model
 *  @author R.SkuLL
*/
final class MarkovModel extends Model {

    const END = '%END%';
    const MAX = 30;

    public $dic;
    public $Db;
    public $start;

    /**
     *  Constructer
    */
    public function __construct () {
        $this->Db = MDB2::singleton();
        $this->dic = array();
    }

    /**
     *  言葉を学習させる
     *  @param string text 覚えさせる文章
    */
    public function Study ($text) {

        // 形態素解析
        $api = new Yahoo_MorphModel;
        $api->Request($text);

        // ３要素より少なかったら覚えない
        if ($api->TotalCount() < 3) return;

        $words = $api->Words();

        // 先頭の言葉を記録
        $this->start = $words->surface;
        $list = array();

        // 解析結果を配列に代入
        foreach ($words as $val) {
            array_push($list, chop($val->surface));
        }

        // 接頭語を２つ生成
        $prifix1 = array_shift($list);
        $prifix2 = array_shift($list);

        // 解析結果を言葉ごとに関連付ける
        foreach ($list as $val) {

            $suffix = $val;
            $this->AddSuffix($prifix1, $prifix2, $suffix);
            $prifix1 = $prifix2;
            $prifix2 = $suffix;

        }

        $this->AddSuffix($prifix1, $prifix2, self::END);
        $this->RegistWord($this->dic);

    }

    /**
     *  解析結果を蓄積させる
     *  @param string prifix1 接頭語1
     *  @param string prifix2 接頭語2
     *  @param string suffix 接尾語
    */
    private function AddSuffix ($prifix1, $prifix2, $suffix) {

        $this->dic[$prifix1][$prifix2] = array();
        array_push($this->dic[$prifix1][$prifix2], $suffix);
        if ($suffix == self::END) {
            $this->dic[$prifix2][$suffix] = array();
        }

    }

    /**
     *  接頭語がDBに登録されているか
     *  @param string prifix 接頭語
     *  @return boolean
    */
    private function CheckPrifix ($prifix) {
        $sql = 'SELECT COUNT(*) FROM markov WHERE prifix = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($prifix))->fetchCol();
        if ($result) return true;
        return false;
    }

    /**
     *  接頭語に続く言葉を取得する
     *  @param string prifix 接頭語
     *  @return array リスト
    */
    private function getSuffix ($prifix) {
        $sql = 'SELECT suffix FROM markov WHERE prifix = ?';
        $req = $this->Db->prepare($sql);
        $result = $req->execute(array($prifix))->fetchCol();
        if ($result) $list = unserialize($result[0]);
        if (!is_array($list)) $list = array();
        return $list;
    }

    /**
     *  解析結果をDBに保存する
     *  @param array list 解析結果のリスト
    */
    private function RegistWord ($list) {

        $ins = 'INSERT INTO markov (prifix, suffix) VALUES (?, ?)';
        $upd = 'UPDATE markov SET suffix = ? WHERE prifix = ?';

        foreach ($list as $key=>$val) {

            $words = array();
            $flg = false;

            if ($words = $this->getSuffix($key)) $flg = true;

            foreach ($val as $k=>$v) {
                array_push($words, $k);
            }

            if ($flg) {
                $words = serialize(array_unique($words));
                $req = $this->Db->prepare($upd);
                $result = $req->execute(array($words, $key));
            } else {
                $words = serialize(array_unique($words));
                $req = $this->Db->prepare($ins);
                $result = $req->execute(array($key, $words));
            }

        }

    }

    /**
     *  マルコフ連鎖で言語生成
     *  @param string text 話しかける言葉
     *  @return string 応答
    */
    public function Generate ($text) {

        // 文章中からキーワードを抽出
        $api = new Yahoo_KeyphraseModel();
        $api->Request($text);
        $keyword = $api->getKeyphrase();

        // キーワドから関連する言葉を選択
        if ($suffix1 = $this->getSuffix($keyword)) {

            $prifix1 = $this->RandomWord($suffix1);

            // Debug
            echo 'Keyword : '.$keyword.'<br>';
            echo '関連 : '.$prifix1;

        } else {

            // 文章中から名詞を抽出
            $api = new Yahoo_MorphModel();
            $api->Request($text, 9);
            $keyword = $api->OneWord();

            // 関連する言葉があるか
            if (!empty($keyword)) {

                $suffix1 = $this->getSuffix($keyword);
                $prifix1 = $this->RandomWord($suffix1);

                // Debug
                echo 'Morph : '.$prifix1;

            } else {

                // なければ話しかけられた最初の言葉
                $prifix1 = $this->start;

                // Debug
                echo 'Key : '.$prifix1;

            }

        }

        if ($prifix1 == self::END) $prifix1 = '';
        $words = array($keyword);

        // 上からさらに関連する言葉を選択
        if ($suffix2 = $this->getSuffix($prifix1)) {
            $prifix2 = $this->RandomWord($suffix2);
            if ($prifix2 == self::END) $prifix2 = '';
        }

        array_push($words, $prifix1, $prifix2);

        // ENDがでるかMAXまで関連する言葉をランダムで生成
        for ($i = 0; $i < self::MAX; $i++) {

            if ($suffix = $this->getSuffix($prifix2)) {
                $suffix = $this->RandomWord($suffix);
            }

            if ($suffix == self::END || empty($suffix)) break;

            array_push($words, $suffix);

            $prifix2 = $suffix;

        }

        // ちゃんと反応がセットされていれば返す
        if ($speak = join('', $words)) return $speak;

        // 何も応答が返らなかった時の応答
        $msg = array(
            'ちょっと何言ってるかわからない',
            'えっと・・？',
            '日本語でおｋ',
            'ちょっと理解できなかった＞＜'
        );
        return $msg[mt_rand(0, count($msg)-1)];

    }

    /**
     *  リストからランダムで言葉を選択
     *  @param array list 言葉のリスト
     *  @return string 選択された言葉
    */
    private function RandomWord ($list) {
        return $list[mt_rand(0, count($list)-1)];
    }

}
