<?php
/**
 *  ReplyDb Model
 *  @author R.SkuLL
*/

class ReplyDbModel extends Model {

    /**
     *  返信済みかをチェックする
     *  @return boolean
    */
    public static function ReplyCheck ($stid) {
        $Db = MDB2::singleton();
        $sql = 'SELECT COUNT(*) FROM reply WHERE status_id = ?';
        $check = $Db->prepare($sql);
        $result = $check->execute(array($stid))->fetchCol();
        return !$result[0] ? true : false;
    }

    /**
     *  返信したツイートを登録
     *  @param Int 返信元ツイートのID
    */
    public static function RegistTweet ($stid) {
        $Db = MDB2::singleton();
        $sql = "INSERT INTO reply (status_id) VALUES ($stid)";
        $Db->exec($sql);
        echo $stid;
    }

}
