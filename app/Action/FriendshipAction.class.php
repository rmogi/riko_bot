<?php
/**
 *  Friendship Action
 *  @author R.SkuLL
*/

final class FriendshipAction extends Action {

    /**
     * 完全相互フォローを実行
    */
    public function execute () {

        // 差を解析
        $Friendship = new FriendshipModel();
        $result = $Friendship->Diff();

        // フォローする
        foreach ($result['follow'] as $val) {
            $Friendship->Twitter->Follow($val, true);
        }

        // リムーブする
        foreach ($result['remove'] as $val) {
            $Friendship->Twitter->Follow($val, false);
        }

    }

}
