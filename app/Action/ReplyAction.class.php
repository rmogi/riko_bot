<?php
/**
 *  Reply Action
 *  @author R.SkuLL
*/

final class ReplyAction extends Action {

    public $Twitter;

    public function __construct () {
        $this->Twitter = TwitterModel::singleton();
    }

    /**
     *  リプライに返信
     *  @param int count 取得するリプライの数
    */
    public function execute ($count = 10) {

        $Reply = new ReplyModel();

        $list = $Reply->ReplyList('mentions', $count);

        foreach ($list as $reply) {

            // 応答を取得
            $text = $Reply->Response($reply);

            // 相手に返信
            $result = $this->Twitter->Post($text, $reply['status_id']);

            if (empty($result->error)) {
                echo $text;
                ReplyDbModel::RegistTweet($reply['status_id']);
            } else {
                echo $result->error;
            }

        }

    }

}
