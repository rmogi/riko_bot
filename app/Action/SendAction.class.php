<?php
/**
 *  Send Action
 *  @author R.SkuLL
*/

class SendAction extends Action {

    public function execute () {

        $Send = new SendModel($this->getConfig('Send'));
        $Send->SendReply();

    }

}
