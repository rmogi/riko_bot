<?php
/**
 *  Schedule Action
 *  @author R.SkuLL
*/

final class ScheduleAction extends Action {

    public function execute () {
        $schedule = new ScheduleModel($this->getConfig('Schedule'));
        $schedule->execute();
    }

}
